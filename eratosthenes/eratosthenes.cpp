#include <iostream>

using namespace std;

void sieveOfEratosthenes(int numbers[], int, int);
void fillArray(int numbers[], int length);
int getLength(int);
void displayArray(int numbers[], int);

int main()
{
    const int length = 100;
    int numbers[length]{ 0 };
    int newLength = getLength(length);
    fillArray(numbers, newLength);
    displayArray(numbers, newLength);
    cout << endl;
    sieveOfEratosthenes(numbers, newLength, 2);
    displayArray(numbers, newLength);
}

void displayArray(int numbers[], int length)
{
    for (size_t i = 0; i < length; i++)
    {
        cout << numbers[i] << " ";
    }
}

int getLength(int currentLength)
{
    int newLength = 0;
    cout << "Enter number less than " << currentLength << ": ";
    cin >> newLength;
    return newLength;
}

void fillArray(int numbers[], int length)
{
    for (size_t i = 0; i <= length; i++)
    {
        numbers[i] = i;
    }
}


void sieveOfEratosthenes(int numbers[], int length, int elementIndex)
{
    if (elementIndex <= sqrt(length))
    {
        if (numbers[elementIndex] != 0)
        {
            for (size_t i = elementIndex * 2; i < length; i += elementIndex)
            {
                numbers[i] = 0;
            }
        }

        sieveOfEratosthenes(numbers, length, elementIndex + 1);
    }
    
}